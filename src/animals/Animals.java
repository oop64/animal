/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animals;

/**
 *
 * @author user1
 */
public class Animals {

    protected String name;
    protected int numOfLegs = 0;
    protected String color;

    public Animals(String name, String color, int numOfLegs) {
        System.out.println("Animal created");
        this.name = name;
        this.color = color;
        this.numOfLegs = numOfLegs;
    }

    public void walk() {
        System.out.println("Animal walk");
    }

    public void speak() {
        System.out.println("Animal speak");
        System.out.println("name : " + this.name + " color : " + this.color + " numOfLegs : " + this.numOfLegs);
    }

    public String getName() {
        return name;
    }

    public int getNumOfLegs() {
        return numOfLegs;
    }

    public String getColor() {
        return color;
    }

}
