/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animals;

/**
 *
 * @author user1
 */
public class Dog extends Animals {

    public Dog(String name, String color) {
        super(name, color, 4);
        System.out.println("Dog create");
    }

    @Override
    public void speak() {
        System.out.println("Dog : " + name + " speak > Box! Box!!");
    }

    @Override
    public void walk() {
        super.walk();
        System.out.println("Dog : " + name + " walk with " + numOfLegs + " legs.");
    }
}
