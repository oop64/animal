/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animals;

/**
 *
 * @author user1
 */
public class Cat extends Animals {

    public Cat(String name, String color) {
        super(name, color, 4);
        System.out.println("Cat create");
    }

    @Override
    public void speak() {
        System.out.println("Cat : " + name + " speak > Mewo~ Meow~~");
    }

    @Override
    public void walk() {
        super.walk();
        System.out.println("Cat : " + name + " walk with " + numOfLegs + " legs.");
    }
}
