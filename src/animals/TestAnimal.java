/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animals;

/**
 *
 * @author user1
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animals animal = new Animals("Ani", "White", 0);
        animal.speak();
        animal.walk();
        newLine();

        Dog dang = new Dog("dang", "Black&White");
        dang.speak();
        dang.walk();
        newLine();
        
        Dog to = new Dog("To", "Brown");
        to.speak();
        to.walk();
        newLine();
        
        Dog mome = new Dog("Mome", "Black&White");
        mome.speak();
        mome.walk();
        newLine();
        
        Dog bat = new Dog("Bat", "Black&White");
        bat.speak();
        bat.walk();
        newLine();

        Cat zero = new Cat("Zero", "Orangr");
        zero.speak();
        zero.walk();
        newLine();

        Duck zom = new Duck("Zom", "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        newLine();
        
        Duck gabgab = new Duck("Gabgab", "Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        newLine();
        
//      instanceof
        System.out.println("zom is Animal : "+ (zom instanceof Animals));
        System.out.println("zom is Duck : "+ (zom instanceof Duck));
        System.out.println("zom is Cat : "+ (zom instanceof Object));
        System.out.println("Animal is Dog : "+ (animal instanceof Dog));
        System.out.println("Animal is Animals : "+ (animal instanceof Animals));
        newLine();
        
        Animals ani1 = null;
        Animals ani2 = null;
        ani1=zom;
        ani2=zero;
        
        System.out.println("Ani1 : zom is Duck "+(ani1 instanceof Duck));
        newLine();
        
        Animals[] animals = {dang, zero, zom, mome, gabgab, to, bat};
        for(int i = 0 ; i < animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            newLine();
        }
        
    }

    private static void newLine() {
        System.out.println("");
    }
}
