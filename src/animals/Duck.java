/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animals;

/**
 *
 * @author user1
 */
public class Duck extends Animals {

    private int numOfWings;

    public Duck(String name, String color) {
        super(name, color, 2);
        System.out.println("Duck create");
    }

    public void fly() {
        System.out.println("Duck : " + name + " fly!!!");
    }

    @Override
    public void speak() {
        System.out.println("Duck : " + name + " speak > Quack!!!");
    }

    @Override
    public void walk() {
        super.walk();
        System.out.println("Duck : " + name + " walk with " + numOfLegs + " legs.");
    }
}
